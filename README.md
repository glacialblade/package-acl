# Modular Laravel Structure #

Implement modular on laravel.

## Setup ##
composer require glacialblade/acl

### Add to providers on app.php ###
Glacialblade\Acl\AclServiceProvider::class

### Add to aliases on app.php ###
'Acl' => Glacialblade\Acl\Facades\AclFacade::class

### Generate Config File ###
php artisan acl:config

### Generate Migrations ###
php artisan acl:migrations

### Generate ACL's Middleware ###
php artisan acl:middleware

### Everytime we need to generate query models ###
php artisan acl:queries

### Everytime we want to add new permissions via command ###
php artisan acl:permissions