<?php

return [
	/**
	 * Models to be assigned and used by the Access Control Trait Query
	 */
	'models' => [
		// $this->model->user on App\Models\User\User
		'user' => 'User'
	],

	/**
	 * Permissions for user that is used in generate
	 */
	'permissions' => [
		1 => [
			'dashboard.index'
		]
	]
];