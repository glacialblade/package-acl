<?php namespace Glacialblade\Acl\Traits;

use Acl;

/**
 * Models Access
 *
 * Class AclModelControl
 * @package Glacialblade\Acl\Traits
 */
trait AclModelControl {

	public $model;

	public function __construct() {
		$this->model = (object) [];

		foreach(config('acl.models') as $name=>$class) {
			$this->model->$name = Acl::getQueryClass($class);
		}
	}

}