<?php namespace Glacialblade\Acl\Facades;

use Illuminate\Support\Facades\Facade;

class AclFacade extends Facade {

	protected static function getFacadeAccessor() {
		return 'Acl';
	}

}