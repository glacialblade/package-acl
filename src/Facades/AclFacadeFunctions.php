<?php namespace Glacialblade\Acl\Facades;

use App\Models\Permission\Permission;
use App\Models\UserType\UserType;
use Exception;
use Illuminate\Support\Facades\URL;

class AclFacadeFunctions {

	public static $multipleAccessRouteRedirect = 'auth.choose.access';

	private static $user;
	private static $permissions;
	private static $sessionUserType = 'access_control.user_type';

/*
 |--------------------------------------------------------------------------
 | Public Methods
 |--------------------------------------------------------------------------
*/
	/**
	 * List of User's User Types
	 *
	 * @return mixed
	 * @throws AccessControlException
	 */
	public static function getUserTypesList() {
		return self::getUserType(true);
	}

	/**
	 * Select a Specific Access
	 * @param $userTypeId
	 *
	 * @return bool
	 */
	public static function chooseAccess($userTypeId) {
		$userType = self::queryUserType($userTypeId, true);

		if($userType) {
			session()->put(self::$sessionUserType, $userType);
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Get Chosen Access.
	 *
	 * @return mixed
	 */
	public static function getChosenAccess() {
		return session()->get(self::$sessionUserType);
	}

	/**
	 * Check if User has Permission on the given access key.
	 * @param $accessKey
	 *
	 * @return bool
	 */
	public static function hasPermission($accessKey) {
		self::setValues();

		$permissions = self::$permissions;

		// Check the access key if it's inside the permissions array.
		if($permissions && in_array($accessKey, $permissions)) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Get Query Class
	 *
	 * @param $model
	 *
	 * @return mixed
	 */
	public static function getQueryClass($model) {
		try {
			// Call the Generic Model Query if User is not logged in.
			$template = '';
			if(auth()->id()) {
				$userType = self::getUserType();
				$template = $userType->template_function;
			}

			$queryClassName = str_replace(' ', '', $template).$model.'Query';
			$queryClass = "\\App\\Models\\$model\\Queries\\$queryClassName";

			return new $queryClass();
		}
		catch(AccessControlException $e) {
			self::exceptionHandler($e);
		}
	}

	/**
	 * Returns template directory.
	 *
	 * @param $module
	 * @param $path
	 *
	 * @return string
	 */
	public static function getTemplate($module, $path) {
		try {
			$userType = self::getUserType();
			$templateFunction = str_replace(' ', '_', strtolower($userType->template_function));

			$module = ucfirst($module);
			return "$module::$templateFunction.$path";
		}
		catch(AccessControlException $e) {
			self::exceptionHandler($e);
		}
	}

	public static function logout() {
		session()->put(self::$sessionUserType, false);
	}
/*
 |--------------------------------------------------------------------------
 | Private Methods
 |--------------------------------------------------------------------------
*/
	/**
	 * Set Permissions
	 */
	private static function setValues() {
		self::$user = auth()->user();

		/**
		 * Set Permissions
		 */
		if(!self::$user) {
			self::$permissions = false;
		}
		else {
			// User Permissions
//			$permissions = Permission::where('user_id', $user->id);

			// User Type permissions
//			$userTypes = UserUserType::where('user_id', $user->id)->get();
//			foreach($userTypes as $userType) {
//				$permissions->orWhere('user_type_id', $userType->user_type_id);
//			}

			$userType = session()->get(self::$sessionUserType);
			if(!$userType) {
				try {
					self::getUserType();
					$userType = session()->get(self::$sessionUserType);
				}
				catch(AccessControlException $e) {
					self::exceptionHandler($e);
				}
			}

			// FETCH PERMISSIONS!
			$permissions = Permission::where('user_type_id', $userType->id)->get();

			// Final Permissions Variable to be Returned.
			$finalPermissions = [];

			// Loop the permissions.
			if(count($permissions) > 0) {
				foreach($permissions as $permission) {
					// Merge the permissions all together, make sure to decode it.
					$finalPermissions = array_merge($finalPermissions, $permission->permission ? json_decode($permission->permission) : []);
				}
			}

			// Set the Permissions!
			self::$permissions = array_unique($finalPermissions);
		}
	}

	/**
	 * Get User Type
	 * @param bool|false $list
	 *
	 * @return mixed
	 * @throws AccessControlException
	 */
	private static function getUserType($list = false) {
		$sessionUserType = session()->get(self::$sessionUserType);

		$userType = !$sessionUserType || $list ? self::queryUserType() : [$sessionUserType];

		// Used in Private
		if(!$list) {
			if(count($userType) == 1) {
				session()->put(self::$sessionUserType, $userType[0]);
				return $userType[0];
			}
			else {
				throw new AccessControlException('Multiple Access');
			}
		}
		// Used in Public
		else {
			return $userType;
		}
	}

	/**
	 * Handles Exception Errors.
	 * @param $e
	 */
	private static function exceptionHandler($e) {
		// Redirects to Choose Access Page.
		if($e->getMessage() == 'Multiple Access') {
			$url = URL::route(self::$multipleAccessRouteRedirect);
			header("Location: $url");

			// Don't Erase
			dd();
		}
	}

	/**
	 * Returns User Types
	 *
	 * @param bool|false $userTypeId
	 * @param bool|false $single
	 *
	 * @return mixed
	 */
	private static function queryUserType($userTypeId = false, $single = false) {
		// Main Query
		$userTypes = UserType::select('user_types.*')
		                     ->leftJoin('user_user_types', 'user_user_types.user_type_id', '=', 'user_types.id')
		                     ->where('user_user_types.user_id', auth()->id());

		// If there is user type id add it to where
		if($userTypeId) {
			$userTypes = $userTypes->where('user_types.id', $userTypeId);
		}

		return !$single ? $userTypes->get() : $userTypes->first();
	}
}

class AccessControlException extends Exception { }