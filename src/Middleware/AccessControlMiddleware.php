<?php

namespace App\Http\Middleware;

use Closure, Acl;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class AccessControlMiddleware {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = null) {
		$accessKey = Route::currentRouteName();

		if(Acl::hasPermission($accessKey)) {
			return $next($request);
		}
		else {
			if(!Auth::id()) {
				return redirect()->route('auth.login');
			}
			else {
				return redirect()->route('dashboard.index');
			}
		}

	}

}
