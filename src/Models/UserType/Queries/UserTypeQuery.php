<?php namespace App\Models\UserType\Queries;

use App\Libraries\Utilities\Utilities;
use App\Models\Permission\Permission;
use App\Models\UserType\UserType;

class UserTypeQuery {

	/**
	 * Single by ID
	 * @param $id
	 *
	 * @return UserType
	 */
	public function single($id) {
		$userType = UserType::find($id);

		return $userType ? $userType : new UserType();
	}

	/**
	 * Get Grouped User Types by an array of ids
	 * @param $ids
	 *
	 * @return mixed
	 */
	public function getGroupedUserTypesByUserIds($ids) {
		return UserType::selectRaw("GROUP_CONCAT(user_types.type SEPARATOR ',') as user_types")
		               ->whereIn('id', $ids)
		               ->first();
	}
}