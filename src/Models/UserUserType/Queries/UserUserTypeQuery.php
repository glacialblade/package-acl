<?php namespace App\Models\UserUserType\Queries;

use App\Libraries\Utilities\Utilities;
use App\Models\User\User;
use App\Models\UserUserType\UserUserType;

class UserUserTypeQuery {

	/**
	 * Get Grouped User Types by User ID.
	 * @param $userId
	 *
	 * @return mixed
	 */
	public function getGroupedUserTypesByUserId($userId) {
		return UserUserType::selectRaw("GROUP_CONCAT(user_types.type SEPARATOR ',') as user_types")
		                   ->leftJoin('user_types', 'user_types.id', '=', 'user_user_types.user_type_id')
		                   ->where('user_user_types.user_id', $userId)
		                   ->first();
	}
}