<?php namespace App\Models\UserUserType;

use Illuminate\Database\Eloquent\Model;

class UserUserType extends Model {

	protected $table = 'user_user_types';

	protected $fillable = [
		'user_type_id',
		'user_id'
	];

}
