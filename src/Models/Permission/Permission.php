<?php namespace App\Models\Permission;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model {

	protected $table = 'permissions';

	protected $fillable = [
		'permission',
		'user_type_id'
	];

}
