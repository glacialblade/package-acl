<?php namespace Glacialblade\Acl\Console\Commands;

use Illuminate\Console\Command;

class AclModelsCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'acl:models';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate Models';

	/**
	 * Create a new command instance.
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 */
	public function fire() {
		$fromPath  = __DIR__.'/../../Models/';
		$modelPath = base_path().'/app/Models/';

		// Loop on Main Model Path
		$modelsDir = opendir($fromPath);
		while($modelName = readdir($modelsDir)) {
			if($modelName != '..' && $modelName != '.') {
				if(!file_exists($modelPath.$modelName)) {
					$this->copy_directory($fromPath.$modelName, $modelPath.$modelName);
				}
			}
		}
	}

	protected function copy_directory( $source, $destination ) {
		if ( is_dir( $source ) ) {
			@mkdir( $destination );
			$directory = dir( $source );
			while ( FALSE !== ( $readdirectory = $directory->read() ) ) {
				if ( $readdirectory == '.' || $readdirectory == '..' ) {
					continue;
				}
				$PathDir = $source . '/' . $readdirectory;
				if ( is_dir( $PathDir ) ) {
					$this->copy_directory( $PathDir, $destination . '/' . $readdirectory );
					continue;
				}
				copy( $PathDir, $destination . '/' . $readdirectory );
			}

			$directory->close();
		}else {
			copy( $source, $destination );
		}
	}

}
