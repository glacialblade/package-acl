<?php

namespace Glacialblade\Acl\Console\Commands;

use Illuminate\Console\Command;

class AclConfigCommand extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'acl:config';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate config file.';


	/**
	 * Create a new command instance.
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire() {
		$source      = __DIR__.'/../../Config/acl.php';
		$destination =  config_path('acl.php');

		if(!file_exists($destination)) {
			copy($source, $destination);
		}
		else {
			$this->info('acl config already exists.');
		}
	}
}