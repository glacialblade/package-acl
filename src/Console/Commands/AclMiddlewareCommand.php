<?php

namespace Glacialblade\Acl\Console\Commands;

use Illuminate\Console\Command;

class AclMiddlewareCommand extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'acl:middleware';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate middleware file.';


	/**
	 * Create a new command instance.
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire() {
		$source      = __DIR__.'/../../Middleware/AccessControlMiddleware.php';
		$destination =  base_path().'/app/Http/Middleware/AccessControlMiddleware.php';


		if(!file_exists($destination)) {
			copy($source, $destination);
		}
		else {
			$this->info('acl config already exists.');
		}
	}
}