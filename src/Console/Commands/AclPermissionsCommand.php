<?php namespace Glacialblade\Acl\Console\Commands;

use App\Models\Permission\Permission;
use Illuminate\Console\Command;

class AclPermissionsCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'acl:permissions';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Updates Permissions User Types';

	/**
	 * Create a new command instance.
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 */
	public function fire() {
		$defaultPermissions = config()->get('acl.permissions');

		foreach($defaultPermissions as $userTypeId=>$permissions) {
			$model = Permission::where('user_type_id', $userTypeId)->first();
			if($model) {
				$model->delete();
			}

			$model = new Permission([
				'user_type_id' => $userTypeId,
				'permission'  => json_encode($permissions)
			]);

			$model->save();
		}
	}

}
