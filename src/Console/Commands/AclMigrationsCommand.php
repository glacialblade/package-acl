<?php

namespace Glacialblade\Acl\Console\Commands;

use Illuminate\Console\Command;

class AclMigrationsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'acl:migrations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate migration files.';


    /**
     * Create a new command instance.
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire() {
        $files = glob(__DIR__.'/../../Migrations/*.php');

        foreach($files as $source) {
            $filename = explode('/', $source);
            $filename = $filename[count($filename) - 1];

            if(!file_exists(base_path().'/database/migrations/'.$filename)) {
                copy($source, base_path().'/database/migrations/'.$filename);
            }
            else {
                $this->info("$filename already exists.");
            }
        }
    }
}