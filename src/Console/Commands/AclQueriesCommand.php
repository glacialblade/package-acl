<?php

namespace Glacialblade\Acl\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AclQueriesCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'acl:queries';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate queries on each model found.';


    /**
     * Create a new command instance.
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire() {
        $modelPath = base_path().'/app/Models/';

        // Loop on Main Model Path
        $modelsDir = opendir($modelPath);
        while($modelName = readdir($modelsDir)) {
            if($modelName != '..' && $modelName != '.') {
                // Add Queries Folder if does not exist.
                $queryPath = $modelPath.$modelName.'/Queries';
                if(!file_exists($queryPath)) {
                    mkdir($queryPath, 0755);
                }

                // Add Base Query File!
                $mainQueryFilePath = $queryPath.'/'.$modelName.'Query.php';
                if(!file_exists($mainQueryFilePath)) {
                    $file = fopen($mainQueryFilePath, 'w');
                    $this->writeMainQueryTemplate($file, $modelName);
                }

                // User Types Query Files!
                $userTypes = DB::table('user_types')->orderBy('id', 'DESC')->get();
                foreach($userTypes as $userType) {
                    $userTypeClassName = $userType->type.$modelName;
                    $userTypeQueryFile = $queryPath.'/'.$userTypeClassName.'Query.php';

                    if(!file_exists($userTypeQueryFile)) {
                        $file = fopen($userTypeQueryFile, 'w');
                        $this->writeUserTypeQueryTemplate($file, $modelName, $userType->type, $modelName.'Query');
                    }
                }
            }
        }
    }

    /**
     * Main Query Content
     * @param $file
     * @param $model
     */
    protected function writeMainQueryTemplate($file, $model) {
        $className = $model.'Query';

        $template = <<<EOF
<?php namespace App\Models\\$model\Queries;

/**
 * Class $className
 * Main Queries for $model
 */
class $className {

}
EOF;

        fwrite($file, $template);
    }

    /**
     * User Type Query Content
     * @param $file
     * @param $className
     */
    protected function writeUserTypeQueryTemplate($file, $model, $userType, $extends) {
        $className = $userType.$model.'Query';

        $template = <<<EOF
<?php namespace App\Models\\$model\Queries;

/**
 * Class $className
 * Specific Query Filters for $userType
 */
class $className extends $extends {

}
EOF;

        fwrite($file, $template);
    }
}