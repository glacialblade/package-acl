<?php

namespace Glacialblade\Acl;

use Glacialblade\Acl\Console\Commands\AclConfigCommand;
use Glacialblade\Acl\Console\Commands\AclMigrationsCommand;
use Glacialblade\Acl\Console\Commands\AclModelsCommand;
use Glacialblade\Acl\Console\Commands\AclPermissionsCommand;
use Glacialblade\Acl\Console\Commands\AclQueriesCommand;
use Glacialblade\Acl\Console\Commands\AclMiddlewareCommand;
use Illuminate\Support\ServiceProvider;

class AclServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerFacades();
		$this->registerCommands();
	}

	/**
	 * Register the command
	 *
	 * @return void
	 */
	protected function registerCommands()
	{
		$this->commands([
			AclMigrationsCommand::class,
			AclQueriesCommand::class,
			AclPermissionsCommand::class,
			AclConfigCommand::class,
			AclMiddlewareCommand::class,
			AclModelsCommand::class,
		]);
	}

	protected function registerFacades() {
		$this->app->bind('Acl', 'Glacialblade\Acl\Facades\AclFacadeFunctions');
	}
}
